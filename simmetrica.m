function res = simmetrica(A)
    % restituisce true se la matrice è simmetrica
    s = size(A);
    n = s(1);
    for i=1:n
       for j=1:n
          if A(i,j)~=A(j,i)
              res = false;
              return;
          end
       end
    end
    res = true;
end

