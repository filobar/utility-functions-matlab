% stima ordine di convergenza (fatta per bene, scritta da me)
% xk sono i numeri della successione

function ordine = my_stima_ordine(x)
    n = length(x);
    z0 = x(n) - x(n-1);
    z1 = x(n-1) - x(n-2);
    z2 = x(n-2) - x(n-3);
    ordine = log(abs(z0/z1)) / log(abs(z1/z2));
end