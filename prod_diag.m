function p = prod_diag(A)
    % prodotto degli elementi sulla diagonale
    p = prod(diag(A));
end

