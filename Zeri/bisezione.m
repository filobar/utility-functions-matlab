function [x,it] = bisezione(fname,a,b)
format long

if sign(fname(a))*sign(fname(b)) == 1
   error('Intervallo non corretto: zero non garantito.') 
end

maxit=1000;
i=1;
fa=fname(a);
fb=fname(b);
mid=a+((b-a)/2);
precision = 10*eps;

while abs(b-a)>=precision && i<maxit
    fa=fname(a);
    fb=fname(b);
    mid=a+((b-a)/2);
    result(i)=mid;
    
    if fname(mid) == 0
       break; 
    elseif sign(fa)*sign(fname(mid)) ~= 1
        b=mid;
    elseif sign(fb)*sign(fname(mid)) ~= 1
        a=mid;
    else
       error('Non sono riuscito a raggiungere lo zero') 
    end
    
    i=i+1;
end

x=mid;
it=i-1;

for i=1:it
   err_rel(i)=abs(result(i)-result(it))/abs(result(it));
end

figure
semilogy([1:1:it],err_rel,'-r')