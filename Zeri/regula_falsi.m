function [x,it] = regula_falsi(fname,a,b)
format long

if sign(fname(a))*sign(fname(b)) == 1
   error('Intervallo non corretto: zero non garantito.') 
end

maxit=1000;
i=1;
fa=fname(a);
fb=fname(b);
next=a-fa*(b-a)/(fb-fa);
precision = 10*eps;
result=zeros(maxit);

while abs(b-a)>=precision && i<maxit
    fa=fname(a);
    fb=fname(b);
    next=a-fa*(b-a)/(fb-fa);
    result(i)=next;
    
    if fname(next) == 0
       break; 
    elseif sign(fa)*sign(fname(next)) ~= 1
        b=next;
    elseif sign(fb)*sign(fname(next)) ~= 1
        a=next;
    else
       error('Non sono riuscito a raggiungere lo zero') 
    end
    
    i=i+1;
end

x=next;
it=i;
err_rel=zeros(it);

for i=1:it
   err_rel(i)=abs(result(i)-result(it))/abs(result(it));
end

figure
semilogy([1:1:it],err_rel,'-r')