function [x,it] = corde(fname,dname,x_0)
format long

maxit=1000;
i=2;
m=dname(x_0);
precision=100*eps;
result=zeros(1,maxit);
result(1)=x_0;

while i<maxit && abs(result(i)-result(i-1)) > precision
    result(i)=result(i-1)-fname(result(i-1))/m;
    
    if fname(result(i)) == 0
       break; 
    end
    
    i=i+1;
end

x=result(i-1);
it=i-1;
err_rel=zeros(it);

for i=1:it
   err_rel(i)=abs(result(i)-result(it))/abs(result(it));
end

figure
semilogy([1:1:it],err_rel,'-r')