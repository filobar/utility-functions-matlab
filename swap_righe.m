function M = swap_righe(M,a,b)
    v = M(a,:);
    M(a,:) = M(b,:);
    M(b,:) = v;
end

