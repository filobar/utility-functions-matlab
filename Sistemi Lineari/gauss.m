function res = gauss(A)
    % controlla se la matrice A e tutte le sue sottomatrici di testa
    % principali hanno determinante diverso da zero
    
    s = size(A);
    if s(1) ~= s(2)
       res=false;
       return;
    end
    n = s(1);
    for i=1:n
        if det(A(1:i,1:i)) == 0
            res = false;
            return;
        end
    end
    res = true;
end
