function [flag,L,U] = fatt_lu(M)
    % personale implementazione della fattorizzazione LU
    % flag è vero se è stato possibile effettuare la fattorizzazione LU
    
    if gauss(M) == false
        flag = false;
        return;
    end
    
    s = size(M);
    n = s(1);
    A = M;
    L = eye(n);
    for k=1:n-1
        if A(k,k)~=0
           m = zeros(1,n);
           m(k+1:n) = A(k+1:n,k)/A(k,k);
           m = m';
           e = zeros(1,n);
           e(k)=1;
           Mk = eye(n) - m*e;
           L = L*inv(Mk);
           A = Mk*A;
        end
    end
    
    U = A;
    flag = true;
end

