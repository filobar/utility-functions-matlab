function res = sylvester(A)
    % Verifica se la matrice A è definita positiva tramite il criterio di
    % Sylvester, ovvero verifica se il determinante di tutti i minori di
    % testa (le sottomatrici "in alto") è strettamente maggiore di 0.
    s = size(A);
    if s(1) ~= s(2)
       res=false;
       return;
    end
    n = s(1);
    for i=1:n
        if det(A(1:i,1:i)) <=0
            res = false;
            return;
        end
    end
    res = true;
end