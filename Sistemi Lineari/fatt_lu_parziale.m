function [flag,P,L,U] = fatt_lu_parziale(M)
    % personale implementazione della fattorizzazione LU
    % con pivoting parziale
    % flag è vero se è stato possibile effettuare la fattorizzazione LU
    % P è la matrice di permutazione
    % P*M=L*U
    
    if gauss(M) == false
        flag = false;
        return;
    end
    
    s = size(M);
    n = s(1);
    A = M;
    P = eye(n);
    L = eye(n);
    
    for k=1:n-1
        % scelgo come pivot il valore massimo sulla colonna k
        pivot = -inf;
        riga_pivot = 0;
        for i=k:n
            if A(i,k) > pivot
               pivot = A(i,k);
               riga_pivot = i;
            end
        end
        
        % scambio la riga del pivot con la riga k
        v = A(k,1:n);
        A(k,1:n) = A(riga_pivot,1:n);
        A(riga_pivot,1:n) = v;
        
        % scambio le stesse righe nella matrice di permutazione P
        v = P(k,1:n);
        P(k,1:n) = P(riga_pivot,1:n);
        P(riga_pivot,1:n) = v;
        
        % il pivot ora è in posizione (k,k)
        % procedo con l'eliminazione delle altre righe
        m = zeros(1,n);
        m(k+1:n) = A(k+1:n,k)/A(k,k);
        m = m';
        e = zeros(1,n);
        e(k)=1;
        Mk = eye(n) - m*e;
        L = L*inv(Mk);
        A = Mk*A;
    end
    
    U = A;
    flag = true;
end

