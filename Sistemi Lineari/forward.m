% Risolve il sistema Ax = b con il metodo forward
% A deve essere triangolare inferiore

function x = forward(A,b)
    s = size(A);
    n = s(1);
    x = zeros(n,1);
    
    for i=1:n
        x(i) = b(i);
        for j=1:i-1
            x(i) = x(i) - A(i,j)*x(j);
        end
        x(i) = x(i) / A(i,i);
    end
end