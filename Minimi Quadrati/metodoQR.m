function [a] = metodoQR(x,y,n)
% INPUT
% x vettore colonna con le ascisse dei punti
% y vettore colonna con le ordinate dei punti 
% n grado del polinomio approssimante
% OUTPUT
% a vettore colonna contenente i coefficienti incogniti
 
l=length(x);
H_completa=vander(x);
H=H_completa(:,l-n:l);

[Q,R]=qr(H);
y1=Q'*y;
a=Usolve(R(1:n+1,:),y1(1:n+1));
