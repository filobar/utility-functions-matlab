function [a]=metodoEN(x,y,n)
% n grado del polinomio approssimante e a vettore dei suoi coefficienti
% Costruzione della matrice rettangolare di Vandermonde
l = length(x);
Hcompleta = vander(x);
H = Hcompleta(:, l - n : l);
% Risolve il sistema delle equazioni normali H0 ? Ha = H0y
% con fattorizzazione di Cholesky
A = H' * H;
b = H' * y;
[R, p] = chol(A);
if p > 0
    disp('A non definita positiva')
    disp('Calcolo soluzione sistema con \')
    a = A \ b;
else
    b1 = Lsolve(R', b);
    a = Usolve(R, b1);
end
