% calcola il polinomio di approssimazione ai minimi quadrati con il metodo
% QRLS.
% x e y sono le coordinate dei punti in input
% grado è il grado desiderato per il polinomio approssimante
% a è il vettore di coefficienti del polinomio approssimante (possono
% essere usati in polyval)

% x e y devono avere stessa lunghezza
% a sarà di lunghezza grado+1

function a = qrls(x,y,grado)
    n = length(x);
    % controllo che y sia un vettore colonna
    s = size(y);
    if s(1)==1 && s(2)>1
        y = y';
    end

    % il sistema è Xa=y
    % X è la matrice di vandermonde con input il vettore x
    % a, la soluzione, è il vettore di coefficienti del polinomio risultante
    % y, il termine noto, è il valore che il polinomio deve assumere in ogni
    % punto
    X = vander(x);
    X = X(:,n-grado:n);
    [Q,R] = qr(X);
    R1 = R(1:grado+1,:);
    y1 = Q'*y;
    yt = y1(1:grado+2);
    % adesso il sistema da risolvere è R1*a=yt
    a = Usolve(R1,yt);
end

