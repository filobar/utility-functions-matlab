function M = swap_colonne(M,a,b)
    v = M(:,a);
    M(:,a) = M(:,b);
    M(:,b) = v;
end