%formula di Simpson composita su N sottointervalli
%definiti da nodi equispaziati.

function I=SimpComp(fname,a,b,N)
h=(b-a)/(2*N);
nodi=[a:h:b];
f=fname(nodi);
I=(f(1) + 2*sum(f(3:2:2*N-1)) + 4*sum(f(2:2:2*N)) + f(2*N+1)) * h/3;