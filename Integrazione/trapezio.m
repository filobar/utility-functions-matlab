function res = trapezio(fname,a,b)
    h = abs(b-a);
    res = (fname(a)+fname(b)) * h /2;
end