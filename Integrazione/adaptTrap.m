% Algoritmo di quadratura adattiva con metodo dei trapezi
% fname è il nome della funzione
% a e b sono gli estremi dell'intervallo in cui valutare l'integrale
% toll è il valore della tolleranza ammessa

% I è il valore approssimato dell'integrale
% N è il numero di sottointervalli utilizzati
% numval è il numero di valutazioni della funzione effettuate

function [I,N,numval] = adaptTrap(fname,a,b,toll)
    s = 2; % perchè è il metodo dei trapezi
    h = (b-a)/2;
    numval = 0;
    N = 2;
    
    % ogni chiamata alla funzione 'trapezio' chiama la funzione due volte
    i_1 = trapezio(fname,a,b);
    i_2 = trapezio(fname,a,a+h) + trapezio(fname,a+h,b);
    err = abs(i_2 - i_1)/(2^s-1);
    numval = numval+6;
    
    if err<=toll
        I = i_2;
        return;
    else
        [primo,n_1,val_1] = adaptTrap(fname,a,a+h,toll);
        [secondo,n_2,val_2] = adaptTrap(fname,a+h,b,toll);
        I = primo + secondo;
        numval = numval + val_1 + val_2;
        N = N + n_1 + n_2;
        return;
    end
end