% calcola l'integrale di polinomio di Newton in [a;b] con la formula di Simpson composita

function [I,N] = simpCompNewton(x,y,a,b,toll)
    c = interpNewton(x,y);
    N = 1;
    h = abs(b-a)/2*N;
    I = h/3 * (horner(c,x,a) + 4*horner(c,x,a+h) + horner(c,x,b));
    
    N = 2;
    h = abs(b-a)/2*N;
    IN = h/3 * (horner(c,x,a) + 4*horner(c,x,a+h) + 2*horner(c,x,a+2*h) + 4*horner(c,x,a+3*h) + horner(c,x,b));
    
    N = 4;
    while abs(IN-I) >= toll
        I = IN;
        h = abs(b-a)/(2*N);
        %nodi = linspace(a,b,2*N+1);
        nodi = [a:h:b];
        ynodi = horner(c,x,nodi);
        % calcolo simpson
        IN = ynodi(1) + 2*sum(ynodi(3:2:2*N-1)) + 4*sum(ynodi(2:2:2*N)) + ynodi(2*N+1);
        IN = IN*h/3;
        N = N*2;
    end
end