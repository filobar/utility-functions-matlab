%formula dei trapezi composita su N sottointervalli 
%definiti da nodi equispaziati

function I=TrapComp(fname,a,b,N)
h=(b-a)/N;
nodi=[a:h:b];
f=fname(nodi);
I=(f(1)+2*sum(f(2:N))+f(N+1))*h/2;
