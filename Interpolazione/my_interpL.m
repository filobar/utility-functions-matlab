% Valuta il polinomio di Lagrange per i punti (x,y) e lo valuta nei punti z

function a = interpL(x,y,z)
    n = length(y);
    nz = length(z);
    a = zeros(1,nz);
    for k=1:nz
        
        for j=1:n
            L = 1;
            for i=1:n
                if i~=j
                    L = L * (z(k)-x(i)) / (x(j)-x(i));
                end
            end
            a(k) = a(k) + y(j) * L;
        end
        
    end
end