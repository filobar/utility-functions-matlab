% valuta il polinomio di Lagrange per i punti di input (x,y) in ognuno dei
% punti z
% x e y devono avere stessa lunghezza
% p avrà stessa lunghezza di z

function p = lagrange(x,y,z)
    n = length(x);
    nz = length(z);
    p = zeros(1,nz);
    
    for k=1:nz
        for j=1:n
            L = 1;
            for i=1:n
                if i~=j
                    L = L * (z(k)-x(i)) / (x(j)-x(i));
                end
            end
            p(k) = p(k) + y(j)*L;
        end
    end
end