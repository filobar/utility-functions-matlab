function a = new2poly(c,xx)
    if length(c) ~= length(xx)
        error("coefficienti e punti devono avere stessa lunghezza");
    end
    
    % costruisco il polinomio con horner
    n = length(c);
    syms pol x
    pol = c(n);
    for i=n-1:-1:1
       pol = c(i) + (x - xx(i))*pol;
    end
    % "semplifico"
    expand(pol);
    % estraggo i coefficienti -> a(i) è il coefficiente di x^(i-1)
    a = coeffs(pol);
    clear pol x;
end

